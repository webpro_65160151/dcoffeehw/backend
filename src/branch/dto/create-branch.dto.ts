import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class CreateBranchDto {
  @IsNotEmpty()
  name: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;
}
